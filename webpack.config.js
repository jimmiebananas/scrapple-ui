var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {

  module: {
    loaders: [
      {
        test: /\.js(x?)$/,
        exclude: /(node_modules|vendor)/,
        loaders: ['babel']
      },
      {
        test: /\.(jpe(g?)|png|gif|svg|ico|cur)$/i,
        loader: 'file-loader?name=images/[name].[ext]'
      },
      {
        test: /\.(eot|otf|ttf|woff|woff2)$/i,
        loader: 'file-loader?name=fonts/[name].[ext]'
      },
      {
        test: /\.s?css$/i,
        loader: 'style!css!sass?' + 'includePaths[]=' + (path.resolve(__dirname, './node_modules'))
      }
    ]
  },

  entry: {
    app: [
        'webpack-dev-server/client?http://0.0.0.0:3000',
        './src/index.js'
    ]
  },

  output: {
    publicPath: 'http://localhost:3000/',
    path: __dirname,
    filename: 'scrapple.js'
  },

  devtool: '#source-map',

  plugins: [
    new HtmlWebpackPlugin({
      title: 'Scrapple Dev',
      filename: 'index.html',
      template: 'src/index.template.ejs',
      favicon: ''
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    })    
  ],

  resolve: {
    root: ['node_modules'],
    extensions: ['', '.js', '.jsx']
  }
};
