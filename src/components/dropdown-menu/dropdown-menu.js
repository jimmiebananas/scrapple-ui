import React, { Component } from 'react';
import './_dropdown-menu.scss';

export default class DropdownMenu extends Component {

    componentWillUnmount = () => {
        const { closeHandler, isOpen } = this.props;
        if (closeHandler && isOpen) {
            closeHandler();
        }
    }

    handleMouseLeave = () => {
        const { closeHandler, isOpen } = this.props;
        if (closeHandler && isOpen) {
            setTimeout(closeHandler, 200);
        }
    }

    render() {
        const { children, styleClassName, isOpen, headerComponent: Header, toggleHandler } = this.props;
        let classes = 'dropdown-menu';

        return (
            <div className={classes} onMouseLeave={this.handleMouseLeave}>
                <div className={`${classes}__header-wrapper`} onClick={toggleHandler} >
                    <Header />
                </div>
                {!isOpen ? null :
                    children
                }
            </div>
        );
    }
}