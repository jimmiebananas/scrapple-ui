import React, { Component } from 'react';
import './_dropdown-menu.scss';

export default class DropdownMenuOption extends Component {

    handleClicked = (clickEvent) => {
        const { clickHandler } = this.props;
        
        if (clickHandler) {
            clickHandler();
        }
    }

    render() {
        const { text, styleClassName } = this.props;
        let classes = 'dropdown-menu-option';

        return (
            <div className={classes} onClick={this.handleClicked} >                
                <div>{text}</div>
            </div>
        );
    }
}