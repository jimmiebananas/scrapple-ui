import React from 'react';
import './_office-info-menu.scss';

export default function OfficeInfoMenu(props) {
    const classes = 'office-info-menu';
    const  itemClass = `${classes}__item`;
    const activeClass = `${itemClass} ${itemClass}--active`;
    
    const { menuItemSelected, selectedMenuItem, menuItemNames } = props;
    
    return (
        <div className={classes}>
            {menuItemNames.map(name => {
                return (
                    <OfficeInfoMenuItem 
                        name={name}
                        handler={() => menuItemSelected(name)}
                        active={name === selectedMenuItem}
                        key={name}
                    />
                );
            })}
        </div>
    );
}

const OfficeInfoMenuItem = ({name, active, handler}) => {
    let classes = 'office-info-menu__item';
    if (active) {
        classes += ' ' + classes + '--active';
    }
    return (
        <div className={classes} onClick={handler}>
            <div className='office-info-menu__item-text'>{name}</div>
        </div>
    );
}