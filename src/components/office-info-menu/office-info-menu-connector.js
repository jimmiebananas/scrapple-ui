import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import OfficeInfoMenu from './office-info-menu';
import * as officeInfoMenuActions from '../../actions/office-info-menu-action-creators';

function select(state) {
    return {
        officeInfoMenu: state.officeInfoMenu
    };
}

class OfficeInfoMenuConnector extends Component {

    render() {
        const { dispatch } = this.props;

        return (
            <OfficeInfoMenu
                {...bindActionCreators(officeInfoMenuActions, dispatch)}
                {...this.props.officeInfoMenu}
            />
        );
    }

}

export default connect(select)(OfficeInfoMenuConnector);