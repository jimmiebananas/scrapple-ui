import React, { Component } from 'react';
import './_main-nav.scss';
import UserMenuDropdownConnector from '../user-menu-dropdown/user-menu-dropdown-connector';
import OfficeInfoMenuConnector from '../../components/office-info-menu/office-info-menu-connector';

export default class MainNav extends Component {

    render() {
        const { styleClassName } = this.props;
        let classes = 'main-nav';

        return (
            <div className={classes}>
                <div className='office-info'>
                    <OfficeInfoMenuConnector />
                </div>
                <div className='user-dropdown'>
                    <UserMenuDropdownConnector />
                </div>
            </div>
        );
    }
}