import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { routes } from '../../constants/global-constants';
import UserMenuDropdown from './user-menu-dropdown';
import * as userActions from '../../actions/user-action-creators';
import * as userMenuActions from '../../actions/user-menu-action-creators';

function select(state) {

    return {
        userMenu: state.userMenu,
        user: state.user
    };
}

class UserMenuDropdownConnector extends Component {

    render() {
        const { dispatch, user: { isLoggedIn } } = this.props;

        return (
            <UserMenuDropdown
                {...bindActionCreators(userActions, dispatch)}
                {...bindActionCreators(userMenuActions, dispatch)}
                {...this.props.userMenu}
            />
        );
    }

}

export default connect(select)(UserMenuDropdownConnector);