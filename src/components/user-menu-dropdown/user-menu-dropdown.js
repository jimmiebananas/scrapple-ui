import React, { Component } from 'react';
import './_user-menu-dropdown.scss';
import DropdownMenu from '../dropdown-menu/dropdown-menu';
import DropdownMenuOption from '../dropdown-menu/dropdown-menu-option';
import UserMenuHeader from './user-menu-header';

export default class UserMenuDropdown extends Component {

    handleLogoutClicked = () => {
        const { logoutSubmitted } = this.props;
        logoutSubmitted();
    }

    handleDropdownClicked = () => {
        const { userMenuDropdownToggled, isUserMenuDropdownOpen } = this.props;
        userMenuDropdownToggled();
    }

    render() {
        const { styleClassName, isUserMenuDropdownOpen, userMenuDropdownClosed, userMenuDropdownToggled } = this.props;
        let classes = 'user-menu';

        return (
            <div className={`${classes}__wrapper`} >
                <div className={`${classes}__dropdown`} >
                    <DropdownMenu 
                        isOpen={isUserMenuDropdownOpen}
                        toggleHandler={userMenuDropdownToggled}
                        closeHandler={userMenuDropdownClosed}
                        headerComponent={UserMenuHeader}
                    >
                        <DropdownMenuOption 
                            text="Logout"
                            clickHandler={this.handleLogoutClicked}
                        />
                    </DropdownMenu>
                </div>
            </div>
        );
    }
}