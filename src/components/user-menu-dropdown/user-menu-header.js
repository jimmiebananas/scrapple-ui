import React from 'react';

export default function UserMenuHeader(props) {
    return (
        <div className='user-menu-header'>
            <svg height="50" width="70">
                <circle cx={25} cy={25} r={22} fill="transparent" stroke="white" strokeWidth={3}/>
                <circle cx={25} cy={14} r={7} fill="transparent" stroke="white" strokeWidth={3} />
                <path d="M10 35 L40 35 A 1 1, 0, 0, 0, 10 35" stroke="white" strokeWidth={3} fill="transparent"/>
                <path d="M52 25 L66 25 L59 35 L52 25" fill="white" />
            </svg>
        </div>
    );
};