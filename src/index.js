
/* React  and Redux */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configure-store';
import createBrowserHistory from 'history/createBrowserHistory'
import { BrowserRouter } from 'react-router-dom';

/* Application-specific dependencies */
import { AppRouterConnector } from './views';
import configureAjax from './utils/configure-ajax';

import * as images from './assets/images/';

import './api-utils/ajax-setup';
import './assets/styles/style.scss';

const history = createBrowserHistory({
   basename: document.getElementsByTagName('base')[0].getAttribute('href')
});

configureAjax(history);

// Creates the top-level application store with middleware. Exports the store
// for use in other modules.
const store = configureStore(history);

// Renders the application to the DOM
ReactDOM.render(
    <Provider store={store}>
    <BrowserRouter>
        <AppRouterConnector store={store} history={history}/>
    </BrowserRouter>
    </Provider>,
    document.getElementById('scrapple-wrapper')
);

