import createReducer from '../utils/create-reducer';
import { loginActionTypes } from '../constants/login-action-types';
import { globalConstants } from '../constants/global-constants';

const initialState = {
    isLoggedIn: false,
    errorMessage: null
};

export default createReducer(initialState, {

    [loginActionTypes.LOGIN_SUBMITTED]: (state, action) => {
        return {
            ...state,
        };
    },

    [loginActionTypes.LOGIN_SUCCEEDED]: (state, action) => {
        return {
            ...state,
            errorMessage: null
        };
    },

    [loginActionTypes.LOGIN_FAILED]: (state, action) => {
        return {
            ...state,
            errorMessage: 'Username / Password not recognized'
        };
    },

    [loginActionTypes.LOGIN_ERROR_CLEARED]: (state, action) => {
        return {
            ...state,
            errorMessage: null
        };
    }

});
