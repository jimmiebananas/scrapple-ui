import createReducer from '../utils/create-reducer';
import { appActionTypes } from '../constants/action-types';
import { userActionTypes } from '../constants/user-action-types';
import { globalConstants } from '../constants/global-constants';
import * as appActions from '../actions/app-actions';

const initialState = {
    isLoading: true
};

export default createReducer(initialState, {

    [appActionTypes.APPLICATION_LOADED]: (state, action) => {
        return {
            ...state,
        };
    },

    [userActionTypes.USER_FETCH_SUCCEEDED]: (state, action) => {
        return {
            ...state,
            isLoading: false
        };
    },

    [userActionTypes.USER_FETCH_FAILED]: (state, action) => {
        return {
            ...state,
            isLoading: false
        };
    }    

});
