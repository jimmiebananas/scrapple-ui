import createReducer from '../utils/create-reducer';
import { userActionTypes } from '../constants/user-action-types';
import { loginActionTypes } from '../constants/login-action-types';

const initialState = {
    isLoggedIn: false,
    userInfo: {}
};

export default createReducer(initialState, {

    [userActionTypes.USER_FETCH_SUCCEEDED]: (state, action) => {
        const { userInfo } = action;
        
        return {
            ...state,
            isLoggedIn: true,
            userInfo
        };
    },

    [userActionTypes.USER_FETCH_FAILED]: (state, action) => {
        
        return {
            ...state,
            isLoggedIn: false,
        };
    },

    [loginActionTypes.LOGIN_SUCCEEDED]: (state, action) => {
        const { userInfo } = action.data;
        
        return {
            ...state,
            isLogged: true,
            userInfo
        };
    },

    [userActionTypes.LOGOUT_SUCCEEDED]: (state, action) => {
        return {
            ...state,
            isLoggedIn: false,
            userInfo: {}
        };
    }

});
