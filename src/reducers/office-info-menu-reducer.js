import createReducer from '../utils/create-reducer';
import { officeInfoMenuActionTypes } from '../constants/office-info-menu-action-types';

const initialState = {
    menuItemNames: ['Regulations', 'Management', 'Drawings', 'Specifications', 'Inspiration', 'Contacts'],
    selectedMenuItem: 'Specifications'
};

export default createReducer(initialState, {

    [officeInfoMenuActionTypes.MENU_ITEM_SELECTED]: (state, action) => {
        const { data: { itemName: selectedMenuItem } } = action;

        return {
            ...state,
            selectedMenuItem,
        };
    }

});
