import createReducer from '../utils/create-reducer';
import { userMenuActionTypes } from '../constants/user-menu-action-types';

const initialState = {
    isUserMenuDropdownOpen: false
};

export default createReducer(initialState, {

    [userMenuActionTypes.USER_MENU_DROPDOWN_TOGGLED]: (state, action) => {
        return {
            ...state,
            isUserMenuDropdownOpen: !state.isUserMenuDropdownOpen
        };
    },

    [userMenuActionTypes.USER_MENU_DROPDOWN_CLOSED]: (state, action) => {
        return {
            ...state,
            isUserMenuDropdownOpen: false
        };
    }

});
