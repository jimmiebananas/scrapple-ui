import { officeInfoMenuActionTypes } from '../constants/office-info-menu-action-types';

export function menuItemSelected(itemName) {
    return {
        type: officeInfoMenuActionTypes.MENU_ITEM_SELECTED,
        data: {
            itemName
        }
    };
}
