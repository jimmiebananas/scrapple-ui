import { userMenuActionTypes } from '../constants/user-menu-action-types';
import * as authenticationApi from '../api-utils/authentication-api';

export function userMenuDropdownToggled() {
    return {
        type: userMenuActionTypes.USER_MENU_DROPDOWN_TOGGLED
    };
}

export function userMenuDropdownClosed() {
    return {
        type: userMenuActionTypes.USER_MENU_DROPDOWN_CLOSED
    };
}
