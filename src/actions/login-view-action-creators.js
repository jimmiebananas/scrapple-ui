import { routes, globalConstants } from '../constants/global-constants';
import * as authenticationApi from '../api-utils/authentication-api';
import { loginActionTypes } from '../constants/login-action-types';

export function loginSubmitted(username, password) {

    return dispatch => {
        dispatch({
            type: loginActionTypes.LOGIN_SUBMITTED,
            username: username
        });

        authenticationApi.submitLogin(username, password)
            .then(
                loginResponse => {
                    dispatch(loginSucceeded(loginResponse));
                },
                errorXhr => {
                    dispatch(loginFailed(username));
                }
            );
    }
}

export function loginSucceeded(loginResponse) {
    const { jwt } = loginResponse;
    sessionStorage.setItem(globalConstants.JWT_STORAGE_KEY, jwt);

    return {
        type: loginActionTypes.LOGIN_SUCCEEDED,
        data: {
            ...loginResponse
        },
        meta: {
            redirectTo: routes.HOME
        }
    };
}

export function loginFailed(username) {
    sessionStorage.setItem(globalConstants.JWT_STORAGE_KEY, '');

    return {
        type: loginActionTypes.LOGIN_FAILED,
        data: { username }
    };
}

export function loginErrorCleared() {
    return {
        type: loginActionTypes.LOGIN_ERROR_CLEARED
    };
}
