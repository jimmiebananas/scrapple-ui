import $ from 'jquery';
import { appActionTypes } from '../constants/action-types';
import * as userActions from '../actions/user-action-creators';
import { fetchCurrentUser } from '../api-utils/authentication-api';
import config from '../utils/config';
import { loadConfig } from '../api-utils/config-api';

export function loadedApplication(data) {

    return dispatch => {
        loadConfig().then(() => {
            dispatch({
                type: appActionTypes.APPLICATION_LOADED
            });
        
            fetchCurrentUser().then((userInfo, reject) => {
                dispatch(userActions.userFetchSucceeded(userInfo));
            }).catch((error) => {
                dispatch(userActions.userFetchFailed());
            });
        })
        .catch(() => {
            console.log('No config.json file found. Using defaults from config-api.js');
        });
    }
}
