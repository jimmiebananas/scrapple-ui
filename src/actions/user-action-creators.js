import { routes } from '../constants/global-constants';
import { userActionTypes } from '../constants/user-action-types';
import * as authenticationApi from '../api-utils/authentication-api';

export function userFetchSucceeded(userInfo) {
    
    return dispatch => {
        dispatch({
            type: userActionTypes.USER_FETCH_SUCCEEDED,
            userInfo
        });
    }
}

export function userFetchFailed() {
    
    return dispatch => {
        dispatch({
            type: userActionTypes.USER_FETCH_FAILED
        });
    }
}

export function logoutSubmitted() {
    return dispatch => {
        dispatch({
            type: userActionTypes.LOGOUT_SUBMITTED
        });

        authenticationApi.logoutCurrentUser()
            .then(
                logoutResponse => {
                    dispatch(logoutSucceeded(logoutResponse));
                },
                errorXhr => {
                    dispatch(logoutFailed());
                }
            );
    };
}

export function logoutSucceeded() {
    return {
        type: userActionTypes.LOGOUT_SUCCEEDED,
        meta: {
            redirectTo: routes.LOGIN
        }
    };
}

export function logoutFailed() {
    return {
        type: userActionTypes.LOGOUT_FAILED
    };
}