import $ from 'jquery';
import config from '../utils/config';
import { getConfig } from '../api-utils/config-api';

export function submitLogin(username, password) {

    return new Promise((resolve, reject) => {
        $.ajax(`${getConfig().restUrl}/authenticatedUsers`, {
            method: 'POST',
            data: JSON.stringify({username, password}),
            success: (res, status, xhr) => {
                resolve(res);
            },
            error: (xhr, status, error) => {
                reject(xhr);
            }
        });
    });
}

export function fetchCurrentUser() {
    return new Promise((resolve, reject) => {
        $.ajax(`${getConfig().restUrl}/authenticatedUsers/me`, {
            method: 'GET',
            success: (res, status, xhr) => {
                resolve(res);
            },
            error: (xhr, status, error) => {
                reject(xhr);
            }
        });
    });
}

export function logoutCurrentUser() {
    const conf = getConfig();

    return new Promise((resolve, reject) => {
        $.ajax(`${getConfig().restUrl}/authenticatedUsers`, {
            method: 'DELETE',
            success: (res, status, xhr) => {
                resolve(res);
            },
            error: (xhr, status, error) => {
                reject(xhr);
            }
        })
    });
}
