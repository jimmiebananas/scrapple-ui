import $ from 'jquery';

let config = {
    restUrl: 'http://' + window.location.hostname + ':8080'
};

export function getConfig() {
    return Object.assign({}, config);
}

export function loadConfig() {
    return new Promise((resolve, reject) => {
        $.ajax('config.json', {
            method: 'GET',
            success: (res, status, xhr) => {
                config = Object.assign(config, res);
                resolve(getConfig());
            },
            error: (xhr, status, error) => {
                resolve(getConfig());
            }
        });
    });
}