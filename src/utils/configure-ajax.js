import $ from 'jquery';

function configureAjax(history) {
    $.ajaxSetup({
        beforeSend: function(jqXHR, settings) {
            jqXHR.setRequestHeader('Authorization', 'Bearer ' + sessionStorage.getItem('scrappleJwt'));
            
            // Mark the login request specifically so that our 401 handler can
            // determine that it doesn't need to send the redirect as a result
            if (settings.url.endsWith('authenticatedUsers') && settings.method === 'POST') {
                jqXHR.isLoginRequest = true;
            }
        },
        statusCode: {
            // All 401 responses send the user to login, except the login request
            // itself, which should be coming from the login page
            401: function(jqXHR) {
                if (!jqXHR.isLoginRequest) {
                    history.replace('/login');
                }
            }
        }
    })
}

export default configureAjax;