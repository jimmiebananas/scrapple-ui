export const globalConstants = {
    JWT_STORAGE_KEY: 'scrappleJwt'
};

export const routes = {
    HOME: '/home',
    LOGIN: '/login'
};
