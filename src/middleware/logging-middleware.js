const logger = store => next => action => {
    if (process.env.NODE_ENV !== 'production') {
        console.log('dispatched ====>', action);
    }
    return next(action);
};

export default logger;
