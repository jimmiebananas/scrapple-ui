import React, { Component } from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import * as appActions from '../../actions/app-actions';
import { AppFooter, AppHeader } from '../../components';
import LoginViewConnector from '../../views/login/login-view-connector';
import './_app-layout.scss';


function select(state) {
    return {
        app: state.app,
        login: state.login
    };
}

export class AppLayoutConnector extends Component {

    render() {

        const { children, app: { isLoading } } = this.props;

        return (
            (isLoading) ? null :
            <div className='app-layout'>
                <main id='app-layout' className='app-layout__content'>
                    {children}
                </main>
            </div>
        );
    }

}

export default withRouter(connect(select)(AppLayoutConnector));