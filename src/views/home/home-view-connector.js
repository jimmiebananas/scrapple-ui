import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import HomeView from './home-view';
import MainNav from '../../components/main-nav/main-nav';

function select(state) {

    return {
        app: state.app,
        user: state.user
    };
}

class HomeViewConnector extends Component {

    render() {
        const { dispatch, app, user } = this.props;
        const stateSlices = {
            app,
            user
        };

        return (
            <div>
                <MainNav />
                <HomeView
                    {...stateSlices}
                />
            </div>
        );
    }
}

export default connect(select)(HomeViewConnector);