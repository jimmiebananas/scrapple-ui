import React, { Component } from 'react';
import TextInput from '../../components/text-input/text-input';
import Button from '../../components/button/button';
import './_login-view.scss';

export default class LoginView extends Component {

    handleLoginSubmitted = () => {
        const { loginSubmitted } = this.props;
        const username = this.refs.usernameInput.getValue();
        const password = this.refs.passwordInput.getValue();

        loginSubmitted(username, password);
    };

    handleInputKeyup = (e) => {
        const { loginErrorCleared, errorMessage } = this.props;

        if (e.keyCode === 13) {
            this.handleLoginSubmitted();
        } else if (errorMessage) {
            loginErrorCleared();
        }
    }

    render() {
        const { loginSubmitted, errorMessage } = this.props;
        const nativeInputProps = {
            onKeyUp: this.handleInputKeyup
        };

        return (
            <div className='login-wrapper'>
                <div className='login-logo-wrapper'>
                    <div className='login-logo'>
                        SCRAPL.
                    </div>
                    <div className='login-beta-tag'>
                        [beta]
                    </div>
                </div>
                <div className='login-right'>
                    <div className='login-desc-wrapper'>
                        <div className='login-desc'>
                            <div className='login-desc__header'>
                                It's not just for breakfast anymore!
                            </div>
                            <div className='login-desc__desc'>
                                Scrapl is a top-secret project aimed to bring managing architecture projects to the 21st century. 
                                We're currently developing and testing new and exciting features. 
                                If you'd like to learn more about what we're up to or if you'd like to get early access, 
                                please refer to the contact info at the bottom of the page.
                            </div>
                        </div>
                        <div className='login-form'>
                            <TextInput
                                errorMsg={errorMessage}
                                placeholderText='USERNAME'
                                ref='usernameInput'
                                nativeProps={nativeInputProps}
                            />
                            <TextInput
                                placeholderText='PASSWORD'
                                type='password'
                                ref='passwordInput'
                                nativeProps={nativeInputProps}
                            />
                            <Button
                                styleClassName='login-btn'
                                text='LOG IN'
                                clickHandler={this.handleLoginSubmitted}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}