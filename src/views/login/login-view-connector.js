import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { routes } from '../../constants/global-constants';
import LoginView from './login-view';
import * as loginActions from '../../actions/login-view-action-creators';

function select(state) {

    return {
        user: state.user,
        login: state.login
    };
}

class LoginViewConnector extends Component {

    render() {
        const { dispatch, user: { isLoggedIn } } = this.props;

        return (
            (isLoggedIn) ? <Redirect to={routes.HOME} /> :
                <LoginView
                    {...bindActionCreators(loginActions, dispatch)}
                    {...this.props.login}
                />
        );
    }

}

export default connect(select)(LoginViewConnector);