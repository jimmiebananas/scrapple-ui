import React, { Component } from 'react';
import { Router, Route, Redirect, Switch, IndexRoute, NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { routes } from '../constants/global-constants';
import AppLayoutConnector from './app-layout/app-layout-connector';
import LoginViewConnector from './login/login-view-connector';
import HomeViewConnector from './home/home-view-connector';
import * as appActions from '../actions/app-actions';
import * as components from '../components';
import * as views from '../views';

export default class AppRouterConnector extends Component {

    componentDidMount() {
        const { store: { dispatch } } = this.props;
        dispatch(appActions.loadedApplication());
    }

    render() {
        const { history } = this.props;

        return (
            <Router history={history}>                
                <AppLayoutConnector>
                    <Switch>
                        <Redirect exact from='/' to='/login' />
                        <Route exact path='/login' component={LoginViewConnector} />
                        <Route exact path={routes.HOME} component={HomeViewConnector} />
                    </Switch>
                </AppLayoutConnector>
            </Router>
        );
    }
}