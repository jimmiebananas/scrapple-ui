import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';
import loggingMiddleware from '../middleware/logging-middleware';
import redirectMiddleware from '../middleware/redirect-middleware';
import * as reducers from '../reducers';

// Combine all reducers into a single reducer for Redux to run
const reducer = combineReducers({
    ...reducers
});

// Compose the store function with middleware. Returns a function that will
// instantiate the store.
const composeStoreWithMiddleware = history => compose(
    applyMiddleware(
        reduxThunk.withExtraArgument(history),
        loggingMiddleware,
        redirectMiddleware(history)
    )
)(createStore)(reducer);

export default function configureStore(history) {
    // Initialize the store with the top-level reducer function
    return composeStoreWithMiddleware(history);
}
